class App {
  String appName;
  String category;
  String developer;
  int year;

  App(this.appName, this.category, this.developer, this.year);

  String toCapital(String name) {
    name = appName.toUpperCase();
    return name;
  }
}

void main() {
  var AppWinner = new App('Ambani', 'Education', 'Mukundi Lambani', 2021);
  print("The winning app is " +
      AppWinner.appName +
      " under the " +
      AppWinner.category +
      " category " +
      "developed by " +
      AppWinner.developer);
  print(AppWinner.year);
  var toCaps = AppWinner.toCapital('Ambani');
  print(toCaps);
}
