String? myfunction(String name, String favoriteApp, String city) {
  return ("My name is " +
      name +
      " " +
      "from " +
      city +
      " and my favourite app is " +
      favoriteApp);
}

void main() {
  print(myfunction('Gontse', 'WhatsApp', 'Durban'));
}
